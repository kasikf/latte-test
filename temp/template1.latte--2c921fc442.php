<?php

use Latte\Runtime as LR;

/** source: template/template1.latte */
final class Template2c921fc442 extends Latte\Runtime\Template
{
	public const Source = 'template/template1.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<div>';
		echo LR\Filters::escapeHtmlText($items2) /* line 1 */;
		echo '</div>
<div>';
		echo LR\Filters::escapeHtmlText($items1[0]) /* line 2 */;
		echo '</div>

<h2>Výpis foreach</h2>
';
		if ($items1) /* line 5 */ {
			echo '<ul>
';
			foreach ($items1 as $item) /* line 6 */ {
				echo '        <li>';
				echo LR\Filters::escapeHtmlText($item) /* line 7 */;
				echo '</li>
';

			}

			echo '</ul>
';
		}
		echo '
<h2>Výpis n:foreach</h2>
';
		if ($items1) /* line 12 */ {
			echo '<ul>
';
			foreach ($items1 as $item) /* line 13 */ {
				echo '    <li>';
				echo LR\Filters::escapeHtmlText($item) /* line 13 */;
				echo '</li>
';

			}

			echo '</ul>';
		}
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['item' => '6, 13'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
